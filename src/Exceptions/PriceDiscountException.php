<?php

namespace VmdCms\Modules\Prices\Exceptions;

use VmdCms\CoreCms\Exceptions\CoreException;

class PriceDiscountException extends CoreException
{

}
