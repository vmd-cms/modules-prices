<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use VmdCms\Modules\Prices\Services\CoreRouter;


Route::group([
    'prefix' => \VmdCms\CoreCms\Services\CoreRouter::getInstance()->getUrlPrefix(),
    'middleware' => [
        \Illuminate\Session\Middleware\StartSession::class
    ]
],function (Router $router){
    Route::group([
        'namespace' => "App\\Modules\\Prices\\Controllers",
        'as' => CoreRouter::ADMIN_PRICE_PREFIX,
    ], function (Router $router){

        $router->post('/price/product-price-groups/taxonomies/check-unique', [
            'as' => CoreRouter::CHECK_UNIQUE_PRODUCT_PRICE_GROUP_TAXONOMIES,
            'uses' => 'PriceController@checkUniqueProductPriceGroupTaxonomies'
        ]);
    });
});
