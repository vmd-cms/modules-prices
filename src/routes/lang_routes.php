<?php
use Illuminate\Support\Facades\Route;
use VmdCms\Modules\Prices\Services\PriceRouter;
use Illuminate\Routing\Router;

return function (){
    Route::group([
        'namespace' => "App\\Modules\\Prices\\Controllers",
    ],function (Router $router){
        $router->post('set-currency/{slug}', [
            'as'   => PriceRouter::ROUTE_SET_CURRENCY,
            'uses' => 'CurrencyController@setCurrency',
        ])->withoutMiddleware([\VmdCms\CoreCms\CoreModules\Content\Middleware\DataShareMiddleware::class]);
    });
};
