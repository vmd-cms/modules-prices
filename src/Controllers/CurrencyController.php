<?php

namespace VmdCms\Modules\Prices\Controllers;

use Illuminate\Support\Facades\Validator;
use VmdCms\CoreCms\Controllers\CoreController;
use VmdCms\CoreCms\Services\Responses\ApiResponse;
use VmdCms\Modules\Prices\Entity\Currencies;

class CurrencyController extends CoreController
{
    public function setCurrency($slug)
    {
        $validator = Validator::make(['slug' => $slug],[
            'slug' => 'required|max:5'
        ]);

        if ($validator->fails()) {
            return ApiResponse::error(['errors' => $validator->errors()->toArray()]);
        }

        try {
            Currencies::getInstance()->setCurrencyBySlug($slug);
        }catch (\Exception $exception){
            return ApiResponse::error([]);
        }
        return ApiResponse::success(['success' => true]);
    }
}
