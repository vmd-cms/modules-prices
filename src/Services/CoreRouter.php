<?php

namespace VmdCms\Modules\Prices\Services;

final class CoreRouter
{
    const ADMIN_PRICE_PREFIX = 'adminPrice.';

    const CHECK_UNIQUE_PRODUCT_PRICE_GROUP_TAXONOMIES = 'check_unique_product_price_group_taxonomies';

    const ROUTE_CHECK_UNIQUE_PRODUCT_PRICE_GROUP_TAXONOMIES = self::ADMIN_PRICE_PREFIX . self::CHECK_UNIQUE_PRODUCT_PRICE_GROUP_TAXONOMIES;
}
