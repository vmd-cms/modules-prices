<?php

namespace VmdCms\Modules\Prices\Collections;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\Modules\Prices\Contracts\PriceDTOCollectionInterface;
use VmdCms\Modules\Prices\Contracts\PriceDTOInterface;

class PriceDTOCollection extends CoreCollectionAbstract implements PriceDTOCollectionInterface
{
    /**
     * @param PriceDTOInterface $dto
     */
    public function append(PriceDTOInterface $dto)
    {
        $this->collection->add($dto);
    }
}
