<?php

namespace VmdCms\Modules\Prices\Collections;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\Modules\Prices\Contracts\CurrencyDTOCollectionInterface;
use VmdCms\Modules\Prices\Contracts\CurrencyDTOInterface;
use VmdCms\Modules\Prices\Contracts\PriceDTOCollectionInterface;
use VmdCms\Modules\Prices\Contracts\PriceDTOInterface;

class CurrencyDTOCollection extends CoreCollectionAbstract implements CurrencyDTOCollectionInterface
{
    /**
     * @param CurrencyDTOInterface $dto
     */
    public function append(CurrencyDTOInterface $dto)
    {
        $this->collection->add($dto);
    }
}
