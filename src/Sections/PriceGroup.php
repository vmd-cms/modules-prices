<?php

namespace VmdCms\Modules\Prices\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Models\CmsSection;

class PriceGroup extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'price_groups';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return "Группы Цен";
    }

    public function display()
    {
        return Display::dataTable([
            Column::text('id','ID'),
            Column::text('slug','Slug'),
            Column::text('info.title','Название'),
            Column::text('discount_percent','Скидка %'),
            Column::text('active', 'Состояние'),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return Form::panel([
            FormComponent::switch('active','Active'),
            FormComponent::input('slug','Slug')->maxLength(128)->unique()->required(),
            FormComponent::input('info.title','Название')->maxLength(128)->required(),
            FormComponent::text('info.description','Описание'),
            FormComponent::input('discount_percent','Скидка (%)')->float()->min(0)->max(100),
        ]);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::switch('active','Active'),
            FormComponent::input('slug','Slug')->setDisabled(true),
            FormComponent::input('info.title','Название')->maxLength(128)->required(),
            FormComponent::text('info.description','Описание'),
            FormComponent::input('discount_percent','Скидка (%)')->float()->min(0)->max(100),
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Prices\Models\PriceGroup::class;
    }

    public function isDeletable(CmsModel $model = null): bool
    {
        return !($model instanceof \VmdCms\Modules\Prices\Models\PriceGroup && $model->isDefault());
    }
}
