<?php

namespace VmdCms\Modules\Prices\Models;

use App\Modules\Taxonomies\Models\Filter;
use App\Modules\Taxonomies\Models\Taxonomy;
use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\CmsRelatedModelInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\Modules\Products\Models\Product;

class Price extends CmsModel implements ActivableInterface, CmsRelatedModelInterface
{
    use Activable;

    public static function table(): string
    {
        return 'prices';
    }

    public static function getForeignField(): string
    {
        return 'products_id';
    }

    public static function getBaseModelClass(): string
    {
        return Product::class;
    }

    public function priceTaxonomy()
    {
        return $this->hasMany(PriceTaxonomy::class,'prices_id','id');
    }

    public function taxonomies()
    {
        return $this->hasManyThrough(Taxonomy::class,PriceTaxonomy::class,'prices_id','id','id','taxonomies_id');
    }

    public function filters()
    {
        return $this->hasManyThrough(Filter::class,PriceTaxonomy::class,'prices_id','id','id','taxonomies_id')
            ->with(['info','parent','children']);
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'products_id','id')->with('settings');
    }
}
