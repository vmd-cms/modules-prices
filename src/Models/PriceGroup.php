<?php

namespace VmdCms\Modules\Prices\Models;

use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\HasInfo;

class PriceGroup extends CmsModel implements ActivableInterface, HasInfoInterface
{
    use HasInfo, Activable;

    const DEFAULT = 'default';

    public static function table(): string
    {
        return 'price_groups';
    }

    public function isDefault()
    {
        return $this->slug === self::DEFAULT;
    }

    public function prices()
    {
        return $this->hasMany(Price::class,'price_groups_id','id')->orderBy('order');
    }
}
