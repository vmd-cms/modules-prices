<?php

namespace VmdCms\Modules\Prices\Models;

use VmdCms\CoreCms\Models\CmsInfoModel;

class CurrencyInfo extends CmsInfoModel
{
    public static function table(): string
    {
        return 'currencies_info';
    }
}
