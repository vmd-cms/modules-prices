<?php

namespace VmdCms\Modules\Prices\Models;

use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\HasInfo;
use VmdCms\CoreCms\Traits\Models\Orderable;

class Currency extends CmsModel implements ActivableInterface, OrderableInterface, HasInfoInterface
{
    use Activable,Orderable,HasInfo;

    public static function table(): string
    {
       return 'currencies';
    }
}
