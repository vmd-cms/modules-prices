<?php

namespace VmdCms\Modules\Prices\Models;

use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Models\CmsInfoModel;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;

class PriceGroupInfo extends CmsInfoModel
{
    protected $fillable = ['price_groups_id','title'];

    public static function table(): string
    {
        return 'price_groups_info';
    }
}
