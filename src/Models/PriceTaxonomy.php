<?php

namespace VmdCms\Modules\Prices\Models;

use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\Modules\Taxonomies\Models\Taxonomy;

class PriceTaxonomy extends CmsModel
{
    public static function table(): string
    {
        return 'prices_taxonomies';
    }

    public function taxonomy()
    {
        return $this->belongsTo(Taxonomy::class,'taxonomies_id','id');
    }
}
