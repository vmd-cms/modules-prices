<?php
namespace VmdCms\Modules\Prices\database\seeds;

use VmdCms\CoreCms\Initializers\AbstractModuleSeeder;
use App\Modules\Prices\Models\PriceGroupInfo;
use App\Modules\Prices\Sections\Price;
use App\Modules\Prices\Sections\PriceGroup;

class ModuleSeeder extends AbstractModuleSeeder
{

    /**
     * @inheritDoc
     */
    protected function getSectionsAssoc(): array
    {
        return [
            Price::class => "Цены",
            PriceGroup::class => "Группы Цен",
        ];
    }

    /**
     * @inheritDoc
     */
    protected function getNavigationAssoc(): array
    {
        return [
            [
                "slug" => "shop_group",
                "title" => "Магазин",
                "is_group_title" => true,
                "order" => 2,
                "children" => [
                    [
                        "slug" => "price_group",
                        "title" => "Цены",
                        "icon" => "icon icon-order",
                        "children" => [
                            [
                                "slug" => "prices",
                                "title" => "Цены",
                                "section_class" => Price::class
                            ],
                            [
                                "slug" => "price_groups",
                                "title" => "Группы цен",
                                "section_class" => PriceGroup::class
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function seedModelData()
    {
        $this->createItem(\App\Modules\Prices\Models\PriceGroup::DEFAULT, 'Общая цена', 1);
        $this->createItem('regular', 'Постоянные клиенты', 2);
        $this->createItem('wholesale', 'Оптовые клиенты', 3);
        return;
    }

    protected function createItem($slug, $title, $order)
    {
        $model = new \App\Modules\Prices\Models\PriceGroup();
        $model->slug = $slug;
        $model->active = true;
        $model->order = $order;
        $model->title = $title;
        $model->save();

        $modelInfo = new PriceGroupInfo();
        $modelInfo->title = $title;
        $model->info()->save($modelInfo);
    }
}
