<?php

use VmdCms\Modules\Prices\Models\PriceGroup as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePriceGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug',128)->unique();
            $table->string('title',128)->nullable();
            $table->text('description')->nullable();
            $table->boolean('active')->default(true);
            $table->integer('order')->unsigned()->default(1);
            $table->string('import_source_id',128)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
