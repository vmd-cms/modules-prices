<?php

use VmdCms\Modules\Prices\Models\Price as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPricesAddCurrency extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(model::table(), function (Blueprint $table){
            $table->addColumn('integer','currency_id')->unsigned()->default(null)->after('price_groups_id');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('currency_id', model::table() . '_currency_id_fk')
                ->references('id')->on('currencies')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(model::table(), function (Blueprint $table){
            $table->dropColumn('currency_id');
        });
    }
}
