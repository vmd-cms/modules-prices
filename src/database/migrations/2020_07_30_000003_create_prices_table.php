<?php

use VmdCms\Modules\Prices\Models\Price as model;
use VmdCms\Modules\Prices\Models\PriceGroup as group;
use VmdCms\Modules\Products\Models\Product as product;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer(product::table() . '_id')->unsigned();
            $table->integer(group::table() . '_id')->unsigned()->nullable();
            $table->float('price')->unsigned()->default(0);
            $table->integer('quantity')->unsigned()->default(0);
            $table->boolean('active')->default(true);
            $table->integer('order')->unsigned()->default(1);
            $table->float('discount_percent')->unsigned()->default(0);
            $table->float('discount_price')->unsigned()->default(0);
            $table->dateTime('discount_start')->nullable();
            $table->dateTime('discount_end')->nullable();
            $table->string('import_source_id',128)->nullable();
            $table->timestamps();
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign(product::table() . '_id', model::table() . '_' . product::table() . '_id'.'_fk')
                ->references(product::getPrimaryField())->on(product::table())
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign(group::table() . '_id', model::table() . '_' . group::table() . '_id'.'_fk')
                ->references(group::getPrimaryField())->on(group::table())
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
