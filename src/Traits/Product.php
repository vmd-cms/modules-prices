<?php

namespace VmdCms\Modules\Prices\Traits;

trait Product
{
    public function product()
    {
        return $this->belongsTo(\VmdCms\Modules\Products\Models\Product::class,'products_id','id');
    }
}
