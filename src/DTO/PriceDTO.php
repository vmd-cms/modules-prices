<?php

namespace VmdCms\Modules\Prices\DTO;

use Illuminate\Support\Collection;
use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\Modules\Prices\Contracts\PriceDTOInterface;
use VmdCms\Modules\Prices\Entity\PriceEntity;
use VmdCms\Modules\Prices\Exceptions\PriceDiscountException;
use VmdCms\Modules\Prices\Models\Price;
use VmdCms\Modules\Products\DTO\ProductDTO;
use VmdCms\Modules\Taxonomies\Collections\TaxonomyDTOCollection;
use VmdCms\Modules\Taxonomies\Contracts\TaxonomyDTOCollectionInterface;

class PriceDTO implements PriceDTOInterface
{
    /**
     * Basic price in base currency
     * @var float
     */
    protected $basePrice;

    /**
     * Basic price in current currency
     * @var float
     */
    protected $price;

    /**
     * Final price in current currency
     * @var float
     */
    protected $priceFinal;

    /**
     * Basic price in current currency with action
     * @var float
     */
    protected $priceWithAction;

    /**
     * Basic price in current currency with user discount
     * @var float
     */
    protected $priceWithUserDiscount;

    /**
     * Basic price in current currency with user discount
     * @var float
     */
    protected $priceWithUserGroupDiscount;

    protected $priceId;

    protected $hasActionPrice;

    protected $hasUserDiscountPrice;

    protected $hasUserGroupDiscountPrice;

    protected $actionDiscount;

    protected $userDiscount;

    protected $userGroupDiscount;

    protected $quantity;

    protected $stockTitle;

    /**
     * @var TaxonomyDTOCollection|null
     */
    protected $filtersDTOCollection;

    /**
     * @var bool
     */
    protected $availableToBuy;

    /**
     * @var ProductDTO
     */
    protected $productDTO;

    /**
     * @var DiscountDTO
     */
    protected $discountDTO;

    public function __construct(Price $price, bool $setFilters = false, ProductDTO $productDTO = null)
    {
        $this->priceId = $price->id;
        $this->quantity = $price->quantity ?? 0;

        $this->basePrice = PriceEntity::getBasePrice($price);
        $this->price = $this->priceFinal = PriceEntity::convertInCurrency($this->basePrice);

        $this->setActionPrice($price);
        $this->setUserDiscountPrice($price);
        $this->setUserGroupDiscountPrice($price);

        $this->productDTO = $productDTO instanceof ProductDTO ? $productDTO : new ProductDTO($price->product,false);

        $this->discountDTO = new DiscountDTO($price);

        $this->filtersDTOCollection = $setFilters ? (new PriceEntity())->getPriceFiltersDTOCollection($price) : new TaxonomyDTOCollection();

        $this->setStockTitle();

    }

    /**
     * @return ProductDTO
     */
    public function getProductDTO(): ProductDTO
    {
        return $this->productDTO;
    }

    /**
     * @return DiscountDTO|null
     */
    public function getDiscountDTO(): ?DiscountDTO
    {
        return $this->discountDTO;
    }

    /**
     * @return null|int
     */
    public function getPriceId(): ?int
    {
        return $this->priceId;
    }

    /**
     * @return float
     */
    public function getPriceBase(): float
    {
        return $this->basePrice;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function getPriceFinal(): float
    {
        return $this->priceFinal;
    }

    /**
     * @return float
     */
    public function getPriceWithUserDiscount() : float
    {
        return $this->priceWithUserDiscount;
    }

    /**
     * @return float
     */
    public function getPriceWithAction() : float
    {
        return $this->priceWithAction;
    }

    /**
     * @return bool
     */
    public function hasActionPrice() : bool
    {
        return $this->hasActionPrice;
    }

    /**
     * @return bool
     */
    public function hasUserDiscountPrice() : bool
    {
        return $this->hasUserDiscountPrice;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return bool
     */
    public function isAvailableToBuy(): bool
    {
        return $this->availableToBuy;
    }

    /**
     * @return TaxonomyDTOCollectionInterface
     */
    public function getFiltersDTOCollection(): TaxonomyDTOCollectionInterface
    {
        return $this->filtersDTOCollection;
    }

    /**
     * @return Collection|null
     */
    public function getFiltersDTOArr(): ?array
    {
        return $this->filtersDTOCollection instanceof TaxonomyDTOCollection ? $this->filtersDTOCollection->getItems() : null;
    }

    /**
     * @return string|null
     */
    public function getStockTitle(): ?string
    {
        return $this->stockTitle;
    }

    /**
     * @return float
     */
    public function getActionDiscount(): float
    {
        return $this->actionDiscount;
    }

    /**
     * @return float
     */
    public function getUserDiscount(): float
    {
        return $this->userDiscount;
    }

    public function toArray()
    {
        return [
            'priceId' => $this->priceId,
            'basePrice' => $this->basePrice,
            'price' => $this->price,
            'priceFinal' => $this->priceFinal,
            'priceWithAction' => $this->priceWithAction,
            'priceWithUserDiscount' => $this->priceWithUserDiscount,
            'actionDiscount' => $this->actionDiscount,
            'userDiscount' => $this->userDiscount,
            'hasActionPrice' => $this->hasActionPrice,
            'hasUserDiscountPrice' => $this->hasUserDiscountPrice,
            'availableToBuy' => $this->availableToBuy,
            'stock_quantity' => $this->quantity,
            'stockTitle' => $this->stockTitle,
        ];
    }

    /**
     * @param Price|null $price
     */
    protected function setActionPrice(?Price $price = null){

        try {
            if($this->hasUserDiscountPrice || $this->hasUserGroupDiscountPrice) throw new PriceDiscountException();

            $priceWithActionBase = PriceEntity::getPriceWithAction($price);
            $this->priceWithAction = PriceEntity::convertInCurrency($priceWithActionBase);
            $this->hasActionPrice = true;
            $this->actionDiscount = $this->price - $this->priceWithAction;
            $this->priceFinal = $this->priceWithAction;
        }
        catch (PriceDiscountException $exception){
            $this->priceWithAction = $this->price;
            $this->hasActionPrice = false;
            $this->actionDiscount = 0;
        }
    }

    /**
     * @param Price|null $price
     */
    protected function setUserDiscountPrice(?Price $price = null){

        try {
            if($this->hasActionPrice || $this->hasUserGroupDiscountPrice) throw new PriceDiscountException();

            $priceWithUserDiscountBase = PriceEntity::getPriceWithUserDiscount($price);
            $this->priceWithUserDiscount = PriceEntity::convertInCurrency($priceWithUserDiscountBase);
            $this->hasUserDiscountPrice = true;
            $this->userDiscount = $this->price - $this->priceWithUserDiscount;
            $this->priceFinal = $this->priceWithUserDiscount;
        }
        catch (PriceDiscountException $exception){
            $this->priceWithUserDiscount = $this->price;
            $this->hasUserDiscountPrice = false;
            $this->userDiscount = 0;
        }
    }

    /**
     * @param Price|null $price
     */
    protected function setUserGroupDiscountPrice(?Price $price = null){

        try {
            if($this->hasActionPrice || $this->hasUserDiscountPrice) {
                throw new PriceDiscountException();
            }

            $priceWithUserGroupDiscountBase = PriceEntity::getPriceWithUserGroupDiscount($price);
            $this->priceWithUserGroupDiscount = PriceEntity::convertInCurrency($priceWithUserGroupDiscountBase);
            $this->hasUserGroupDiscountPrice = true;
            $this->userGroupDiscount = $this->price - $this->priceWithUserGroupDiscount;
            $this->priceFinal = $this->priceWithUserGroupDiscount;
        }
        catch (PriceDiscountException $exception){
            $this->priceWithUserGroupDiscount = $this->price;
            $this->hasUserGroupDiscountPrice = false;
            $this->userGroupDiscount = 0;
        }
    }

    protected function setStockTitle()
    {
        $this->setAvailableToBuy();
        $this->stockTitle = $this->isAvailableToBuy() ? PriceEntity::IN_STOCK : PriceEntity::NOT_IN_STOCK;
    }

    protected function setAvailableToBuy()
    {
        $this->availableToBuy = $this->quantity > 0;
    }
}
