<?php

namespace VmdCms\Modules\Prices\DTO;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Carbon;
use VmdCms\Modules\Prices\Entity\PriceEntity;
use VmdCms\Modules\Prices\Models\Price;

class DiscountDTO implements Arrayable
{
    /**
     * @var Carbon|null
     */
    protected $startAt;
    protected $finishAt;

    /**
     * @var float|null
     */
    protected $percent;

    /**
     * @var float|null
     */
    protected $baseSum;

    /**
     * @var float|null
     */
    protected $sum;

    /**
     * @var bool
     */
    protected $isValid;

    public function __construct(Price $price)
    {
        $this->startAt = $price->discount_start;
        $this->finishAt = $price->discount_end;
        $this->percent = PriceEntity::getPriceDiscountPercent($price);
        $this->baseSum = PriceEntity::getPriceDiscountSum($price);
        $this->sum = PriceEntity::convertInCurrency($this->baseSum);
        $this->isValid = PriceEntity::isValidDiscount($price);
    }

    /**
     * @return Carbon|null
     */
    public function getStartAt(): ?Carbon
    {
        return $this->startAt;
    }

    /**
     * @return mixed
     */
    public function getFinishAt()
    {
        return $this->finishAt;
    }

    /**
     * @return float|null
     */
    public function getPercent(): ?float
    {
        return $this->percent;
    }

    /**
     * @return float|null
     */
    public function getSum(): ?float
    {
        return $this->sum;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->isValid;
    }

    public function toArray()
    {
        return [
            'start_at' => $this->startAt,
            'finish_at' => $this->finishAt,
            'percent' => $this->percent,
            'sum' => $this->sum,
            'is_valid' => $this->isValid
        ];
    }
}
