<?php

namespace VmdCms\Modules\Prices\DTO;

use VmdCms\Modules\Prices\Contracts\CurrencyDTOInterface;

class CurrencyDTO implements CurrencyDTOInterface
{
    protected $id;
    protected $slug;
    protected $icon;
    protected $rate;
    protected $default;
    protected $active;

    protected $symbol;
    protected $title;
    protected $description;

    public function __construct(\stdClass $data){
        $this->id = $data->id;
        $this->slug = $data->slug;
        $this->icon = $data->icon;
        $this->rate = $data->rate;
        $this->default = $data->default;
        $this->active = $data->active;
        $this->symbol = $data->symbol ?? $data->info->symbol ?? null;
        $this->title = $data->title ?? $data->info->title ?? null;
        $this->description = $data->description ?? $data->info->description ?? null;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @return null|string
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @return float
     */
    public function getRate(): float
    {
        return $this->rate;
    }

    /**
     * @return bool
     */
    public function getDefault(): bool
    {
        return $this->default;
    }

    /**
     * @return bool
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    /**
     * @return string|null
     */
    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'icon' => $this->icon,
            'rate' => $this->rate,
            'default' => $this->default,
            'active' => $this->active,
            'symbol' => $this->symbol,
            'title' => $this->title,
            'description' => $this->description
        ];
    }
}
