<?php

namespace VmdCms\Modules\Prices\Contracts;

use Illuminate\Contracts\Support\Arrayable;

interface CurrencyDTOInterface extends Arrayable
{
    /**
     * CurrencyDTOInterface constructor.
     * @param \stdClass $data
     */
    public function __construct(\stdClass $data);

    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string|null
     */
    public function getSlug(): ?string;

    /**
     * @return null|string
     */
    public function getIcon(): ?string;

    /**
     * @return float
     */
    public function getRate(): float;

    /**
     * @return bool
     */
    public function getDefault(): bool;

    /**
     * @return bool
     */
    public function getActive(): bool;

    /**
     * @return string|null
     */
    public function getSymbol(): ?string;

    /**
     * @return string|null
     */
    public function getTitle(): ?string;
}
