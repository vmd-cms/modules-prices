<?php

namespace VmdCms\Modules\Prices\Contracts;

use VmdCms\CoreCms\Contracts\Collections\CollectionInterface;

interface CurrencyDTOCollectionInterface extends CollectionInterface
{
    /**
     * @param CurrencyDTOInterface $dto
     */
    public function append(CurrencyDTOInterface $dto);

}
