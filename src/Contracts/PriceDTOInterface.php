<?php

namespace VmdCms\Modules\Prices\Contracts;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\Modules\Prices\Models\Price;
use VmdCms\Modules\Taxonomies\Collections\TaxonomyDTOCollection;
use VmdCms\Modules\Taxonomies\Contracts\TaxonomyDTOCollectionInterface;

interface PriceDTOInterface extends Arrayable
{
    /**
     * PriceDTOInterface constructor.
     * @param Price $model
     */
    public function __construct(Price $model);

    /**
     * @return float
     */
    public function getPriceBase(): float;

    /**
     * @return float
     */
    public function getPrice(): float;

    /**
     * @return float
     */
    public function getPriceFinal(): float;
    /**
     * @return float
     */
    public function getPriceWithUserDiscount() : float;

    /**
     * @return float
     */
    public function getPriceWithAction() : float;

    /**
     * @return bool
     */
    public function hasActionPrice() : bool;

    /**
     * @return bool
     */
    public function hasUserDiscountPrice() : bool;

    /**
     * @return int
     */
    public function getQuantity(): int;

    /**
     * @return bool
     */
    public function isAvailableToBuy(): bool;

    /**
     * @return TaxonomyDTOCollectionInterface
     */
    public function getFiltersDTOCollection(): TaxonomyDTOCollectionInterface;
}
