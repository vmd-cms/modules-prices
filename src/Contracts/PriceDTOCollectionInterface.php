<?php

namespace VmdCms\Modules\Prices\Contracts;

use VmdCms\CoreCms\Contracts\Collections\CollectionInterface;

interface PriceDTOCollectionInterface extends CollectionInterface
{
    /**
     * @param PriceDTOInterface $dto
     */
    public function append(PriceDTOInterface $dto);

}
