<?php

namespace VmdCms\Modules\Prices\Entity;

use App\Modules\Content\Entity\Languages;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\Modules\Prices\Collections\CurrencyDTOCollection;
use VmdCms\Modules\Prices\Contracts\CurrencyDTOCollectionInterface;
use VmdCms\Modules\Prices\Contracts\CurrencyDTOInterface;
use VmdCms\Modules\Prices\DTO\CurrencyDTO;
use VmdCms\Modules\Prices\Exceptions\CurrencyException;

class Currencies
{
    const CURRENCY_SLUG = 'currency_slug';

    /**
     * @var Currencies
     */
    protected static $instance;

    /**
     * @var float
     */
    protected static $currentRate;

    /**
     * @var string
     */
    protected static $currentSymbol;

    /**
     * @var Collection
     */
    protected $currencies;

    /**
     * @var array
     */
    protected $currencyDTOAssoc;

    /**
     * @var CurrencyDTOCollectionInterface
     */
    protected $currenciesDTO;

    /**
     * @var CurrencyDTOInterface
     */
    protected $currentCurrencyDTO;

    /**
     * @var CurrencyDTOInterface
     */
    protected $defaultCurrencyDTO;

    /**
     * Currencies constructor.
     * @throws CurrencyException
     */
    protected function __construct()
    {
        $this->setCurrencies();
    }

    /**
     * @return Currencies
     */
    public static function getInstance(): Currencies
    {
        if (!static::$instance) static::$instance = new static();
        return static::$instance;
    }

    /**
     * @return string
     */
    public static function currentSymbol(): string
    {
        if(empty(static::$currentSymbol)){
            static::$currentSymbol = static::getInstance()->getCurrentCurrencyDTO()->getSymbol();
        }
        return static::$currentSymbol;
    }

    /**
     * @return float
     */
    public static function currentRate(): float
    {
        if(empty(static::$currentRate)){
            static::$currentRate = static::getInstance()->getCurrentCurrencyDTO()->getRate();
        }
        return static::$currentRate;
    }

    protected function setCurrencies()
    {
        $this->currenciesDTO = new CurrencyDTOCollection();

        $this->currencies = DB::table('currencies as base')
            ->select(['base.id','base.slug','base.icon','base.rate','base.default','base.active','info.symbol','info.title','info.description'])
            ->join('currencies_info as info',function ($join){
                $join->on('info.currencies_id','=','base.id')
                    ->where('info.lang_key',Languages::getInstance()->getCurrentLocale());
            })
            ->where('base.active',true)
            ->groupBy('base.id')->get()->keyBy('slug');

        if(is_countable($this->currencies) && count($this->currencies)){

            $currentSlug = session()->get(static::CURRENCY_SLUG);

            foreach ($this->currencies as $item){

                $dto = new CurrencyDTO($item);

                if($item->default){
                    $this->defaultCurrencyDTO = $dto;
                }
                if($item->slug === $currentSlug){
                    $this->currentCurrencyDTO = $dto;
                }

                $this->currenciesDTO->append($dto);

                $this->currencyDTOAssoc[$dto->getId()] = $dto;
            }

            if(empty($this->currentCurrencyDTO)){
                $this->currentCurrencyDTO = $this->defaultCurrencyDTO;
            }
        }
    }

    /**
     * @return Collection
     */
    public function getCurrencies()
    {
        return $this->currencies;
    }

    /**
     * @return CurrencyDTOCollectionInterface
     */
    public function getCurrenciesDTO(): CurrencyDTOCollectionInterface
    {
        return $this->currenciesDTO;
    }

    /**
     * @param int $id
     * @return CurrencyDTO
     */
    public function getCurrencyDTOById(int $id): CurrencyDTO
    {
        $currency = $this->currencyDTOAssoc[$id] ?? null;

        if(!$currency instanceof CurrencyDTO){
            throw new CurrencyException('Currency Not Found',404);
        }

        return $currency;
    }

    /**
     * @param int $currencyId
     * @param float $priceValue
     * @return float|int
     * @throws CurrencyException
     */
    public function getPriceValueWithRate(int $currencyId, float $priceValue)
    {
        return $priceValue * $this->getCurrencyDTOById($currencyId)->getRate();
    }

    /**
     * @return ?string
     */
    public function getDefaultCurrencySymbol(): ?string
    {
        return $this->defaultCurrencyDTO instanceof CurrencyDTOInterface ? $this->defaultCurrencyDTO->getSymbol() : null;
    }

    /**
     * @return ?int
     */
    public function getDefaultCurrencyId(): ?int
    {
        return $this->defaultCurrencyDTO instanceof CurrencyDTOInterface ? $this->defaultCurrencyDTO->getId() : null;
    }

    /**
     * @return null|CurrencyDTOInterface
     */
    public function getDefaultCurrencyDTO(): ?CurrencyDTOInterface
    {
        return $this->defaultCurrencyDTO;
    }

    /**
     * @return CurrencyDTOInterface
     */
    public function getCurrentCurrencyDTO(): CurrencyDTOInterface
    {
        return $this->currentCurrencyDTO;
    }

    /**
     * @return ?string
     */
    public function getCurrentCurrencySymbol(): ?string
    {
        return $this->currentCurrencyDTO instanceof CurrencyDTOInterface ? $this->currentCurrencyDTO->getSymbol() : null;
    }

    /**
     * @param string $slug
     * @throws CurrencyException
     */
    public function setCurrencyBySlug(string $slug)
    {
        $currency = $this->currencies[$slug] ?? null;
        if(empty($currency)){
            throw new CurrencyException('Currency not found');
        }
        session()->put(static::CURRENCY_SLUG,$slug);
    }
}
