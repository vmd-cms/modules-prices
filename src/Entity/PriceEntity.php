<?php

namespace VmdCms\Modules\Prices\Entity;

use App\Modules\Prices\DTO\PriceDTO;
use App\Modules\Products\Models\Product;
use App\Modules\Taxonomies\DTO\TaxonomyDTO;
use App\Modules\Taxonomies\Models\Filter;
use App\Modules\Users\Entity\Auth\AuthEntity;
use App\Modules\Users\Models\User;
use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\Modules\Prices\DTO\CurrencyDTO;
use VmdCms\Modules\Prices\Exceptions\PriceDiscountException;
use VmdCms\Modules\Prices\Models\Price;
use VmdCms\Modules\Taxonomies\Collections\TaxonomyDTOCollection;
use VmdCms\Modules\Users\Models\UserGroup;

class PriceEntity
{
    const IN_STOCK = 'in_stock';
    const NOT_IN_STOCK = 'not_in_stock';

    /**
     * @param float $price
     * @return string
     */
    public static function getPriceBlock(float $price)
    {
        return $price . self::getCurrencyBlock();
    }

    /**
     * @return string
     */
    public static function getCurrencyBlock()
    {
        return ' <span class="UA">' . static::getCurrency() . '</span>';
    }

    /**
     * @return string
     */
    public static function getCurrency()
    {
        $defaultCurrency = Currencies::getInstance()->getDefaultCurrencySymbol();
        return $defaultCurrency ?? '';
    }

    /**
     * @param Price|null $price
     * @return mixed
     */
    public static function getBasePrice(?Price $price)
    {
        return $price instanceof Price ? Currencies::getInstance()->getPriceValueWithRate($price->currency_id, $price->price) : 0;
    }

    /**
     * @param Price|null $price
     * @param User|null $orderUser
     * @return false|float|int|mixed
     */
    public static function getPriceWithActionDiscountsFinal(Price $price = null, User $orderUser = null){
        try {
            return static::getPriceWithAction($price);
        }
        catch (PriceDiscountException $exception){}

        try {
            return static::getPriceWithUserDiscount($price,$orderUser);
        }
        catch (PriceDiscountException $exception){}

        return static::getBasePrice($price);
    }

    /**
     * @param Price|null $price
     * @return float
     */
    public static function getPriceWithActionFinal(?Price $price = null){
        try {
            return static::getPriceWithAction($price);
        }
        catch (PriceDiscountException $exception){
            return static::getBasePrice($price);
        }
    }

    /**
     * @var float
     */
    static $currentRate;

    /**
     * @var float
     */
    static $currentSymbol;

    /**
     * @return float
     */
    public static function getCurrencyRate(): float
    {
        if(empty(static::$currentRate)){
            static::$currentRate = Currencies::getInstance()->getCurrentCurrencyDTO()->getRate();
        }
        return static::$currentRate;
    }

    /**
     * @return string|null
     */
    public static function getCurrencySymbol(): ?string
    {
        if(empty(static::$currentSymbol)){
            static::$currentSymbol = Currencies::getInstance()->getCurrentCurrencyDTO()->getSymbol();
        }
        return static::$currentSymbol;
    }

    public static function convertInCurrency(float $defaultPrice): float
    {
        return $defaultPrice > 0 ? ceil(static::getCurrencyRate() * $defaultPrice) : 0;
    }

    /**
     * @param Price|null $price
     * @return float
     * @throws PriceDiscountException
     */
    public static function getPriceWithAction(?Price $price = null)
    {
        $priceValue = self::getBasePrice($price);

        if (empty($priceValue) || $priceValue <= 0) {
            throw new PriceDiscountException();
        }

        static::validatePriceActionPeriod($price);

        if($discount = static::getPriceDiscountPercent($price))
        {
            return ceil($priceValue - $priceValue * ($discount/100));
        }

        if ($discount = static::getPriceDiscountSum($price))
        {
            return ceil($priceValue - $discount);
        }

        throw new PriceDiscountException();
    }

    /**
     * @param Price $price
     * @throws PriceDiscountException
     */
    protected static function validatePriceActionPeriod(Price $price){

        if(!empty($price->discount_start) && $price->discount_start > date('Y-m-d H:i:s'))
        {
            throw new PriceDiscountException();
        }

        if(!empty($price->discount_end) && $price->discount_end < date('Y-m-d H:i:s'))
        {
            throw new PriceDiscountException();
        }
    }

    /**
     * @param Price $price
     * @return float
     */
    public static function getPriceDiscountPercent(Price $price): float{

        return !empty($price->discount_percent) && $price->discount_percent > 0 && $price->discount_percent < 100 ? $price->discount_percent : 0;
    }

    /**
     * @param Price $price
     * @return float
     */
    public static function getPriceDiscountSum(Price $price): float
    {
        $discountPrice = Currencies::getInstance()->getPriceValueWithRate($price->currency_id, $price->discount_price);
        return $discountPrice > 0 && $discountPrice < static::getBasePrice($price) ? $discountPrice : 0;
    }

    public static function isValidDiscount(Price $price): bool
    {
        try {
            $priceValue = self::getBasePrice($price);

            if (empty($priceValue) || $priceValue <= 0) {
                throw new PriceDiscountException();
            }

            static::validatePriceActionPeriod($price);
        }
        catch (\Exception $exception){
            return false;
        }
        return true;
    }

    /**
     * @param Price|null $price
     * @param User|null $orderUser
     * @return false|float
     * @throws PriceDiscountException
     */
    public static function getPriceWithUserDiscount(?Price $price = null, User $orderUser = null)
    {
        $priceValue = self::getBasePrice($price);

        if (empty($priceValue) || $priceValue <= 0) {
            throw new PriceDiscountException();
        }

        $user = $orderUser instanceof User ? $orderUser : auth()->guard('user')->user();

        if(!$user instanceof User) {
            throw new PriceDiscountException();
        }

        if(!$user->confirmed || $user->discount_percent <= 0){
            throw new PriceDiscountException();
        }

        return ceil($priceValue - $priceValue * ($user->discount_percent/100));
    }

    /**
     * @param Price|null $price
     * @param User|null $orderUser
     * @return false|float
     * @throws PriceDiscountException
     */
    public static function getPriceWithUserGroupDiscount(?Price $price = null, User $orderUser = null)
    {
        $priceValue = self::getBasePrice($price);

        if (empty($priceValue) || $priceValue <= 0) {
            throw new PriceDiscountException();
        }

        $user = $orderUser instanceof User ? $orderUser : AuthEntity::getAuthUser();

        $userGroup = $user instanceof \VmdCms\Modules\Users\Models\User ? $user->userGroup : null;
        if(!$userGroup instanceof UserGroup){
            throw new PriceDiscountException();
        }

        if(!$userGroup->active || $userGroup->discount_percent <= 0){
            throw new PriceDiscountException();
        }

        return ceil($priceValue - $priceValue * ($userGroup->discount_percent/100));
    }

    public static function getDiscountString(PriceDTO $priceDTO)
    {
        $discountDTO = $priceDTO->getDiscountDTO();

        if(!$discountDTO || !$discountDTO->isValid())
        {
            return null;
        }

        if($discount = $discountDTO->getPercent())
        {
            return $discount . '%';
        }

        if($discount = $discountDTO->getSum())
        {
            $currencyDTO = Currencies::getInstance()->getCurrentCurrencyDTO();
            return $currencyDTO instanceof CurrencyDTO ? ($discount * $currencyDTO->getRate()) . ' ' . $currencyDTO->getSymbol() : $discount;
        }

        return null;
    }

    public function getPriceFiltersDTOCollection(Price $price){
        $filtersCollection = new TaxonomyDTOCollection();
        $filters = $price->filters;
        if(is_countable($filters) && count($filters))
        {
            $filtersAssoc = [];
            foreach ($filters as $filter)
            {
                $filterGroupCollection = $filtersAssoc[$filter->parent_id] ?? null;
                if(!$filterGroupCollection instanceof TaxonomyDTOCollection){

                    $filtersAssoc[$filter->parent_id] = $filterGroupCollection = new TaxonomyDTOCollection();
                }

                $filterGroupCollection->append(new TaxonomyDTO($filter,false));
            }

            $groupTaxonomy = Filter::whereIn('id',array_keys($filtersAssoc))->with('info')->get();

            foreach ($groupTaxonomy as $filter){
                $filterDTO = new TaxonomyDTO($filter);
                $filterDTO->setChildren($filtersAssoc[$filter->id]);
                $filtersCollection->append($filterDTO);
            }
        }
        return $filtersCollection;
    }

    public function suspendProductPrices(Product $model,array $taxonomyIds){
        try {
            Price::where('products_id',$model->id)->whereHas('priceTaxonomy',function ($q) use ($taxonomyIds){
                $q->whereNotIn('taxonomies_id',$taxonomyIds);
            })->update(['active'=>false]);
        }catch (\Exception $exception){}
    }
}
