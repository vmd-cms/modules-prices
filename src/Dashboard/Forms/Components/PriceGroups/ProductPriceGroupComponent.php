<?php

namespace VmdCms\Modules\Prices\Dashboard\Forms\Components\PriceGroups;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Route;
use VmdCms\CoreCms\Contracts\Collections\FormComponentsCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\CustomFormComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\InnerDataTableInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Models\CmsRelatedModelInterface;
use VmdCms\CoreCms\Dashboard\Forms\Components\Component;
use VmdCms\CoreCms\Dashboard\Forms\Tables\Components\Actions\DeleteAction;
use VmdCms\CoreCms\Dashboard\Forms\Tables\Components\Actions\ExpandAction;
use VmdCms\CoreCms\Dashboard\Forms\Tables\Components\ColumnActions;
use VmdCms\CoreCms\Exceptions\Models\NotCmsRelatedModelException;
use VmdCms\CoreCms\Traits\Dashboard\Forms\InnerDataTable;
use VmdCms\Modules\Prices\Models\Price;
use VmdCms\Modules\Prices\Models\PriceGroup;
use VmdCms\Modules\Prices\Models\PriceTaxonomy;
use VmdCms\Modules\Prices\Services\CoreRouter;
use VmdCms\Modules\Taxonomies\Models\Taxonomy;

class ProductPriceGroupComponent extends Component implements CustomFormComponentInterface, InnerDataTableInterface
{
    use InnerDataTable;

    protected $productModel;

    protected $priceGroupModel;

    protected $params;

    /**
     * @var FormComponentsCollectionInterface
     */
    protected $dataTableColumns;

    /**
     * @param CmsModelInterface|null $productModel
     * @return $this|CustomFormComponentInterface
     */
    public function setProductModel(CmsModelInterface $productModel = null) : CustomFormComponentInterface
    {
        $this->productModel = $productModel;
        return $this;
    }

    /**
     * @param CmsModelInterface|null $priceGroup
     * @return $this|CustomFormComponentInterface
     */
    public function setPriceGroupModel(CmsModelInterface $priceGroup = null) : CustomFormComponentInterface
    {
        $this->priceGroupModel = $priceGroup;
        return $this;
    }

    public function getStoreCallback(): ?callable
    {
        if($this->isDeletedCurrentGroup())
        {
            Price::where('products_id', $this->productModel->id)->where('price_groups_id',$this->priceGroupModel->id)->delete();
            return null;
        }
        return $this->getStoreTableDataCallback();
    }

    /**
     * @return CmsModelInterface|null
     */
    protected function getModel() :?CmsModelInterface
    {
        return $this->priceGroupModel;
    }

    protected function getComponentKey(): string
    {
        return "product-price-group-component";
    }

    protected function getActionsColumn()
    {
        return (new ColumnActions('__actions', 'Действия'))->setCmsModel($this->priceGroupModel)
            ->setComponents([
                new ExpandAction('__expand'),
                new DeleteAction('__delete')
            ])->setWidth(120);
    }

    protected function appendExpandedComponents($relationItems)
    {
        if(!$relationItems instanceof Collection) return;

        foreach ($relationItems as $key=>$relationItem)
        {
            if(isset($this->items[$key])) {
                $this->items[$key]['expanded_show'] = false;
                $this->items[$key]['expanded'] =  $this->getParamsArray($relationItem);
            }
        }
    }

    protected function getEmptyItem()
    {
        return array_merge($this->emptyItem, [
            'expanded_show' => true,
            'expanded' => $this->getParamsArray()
        ]);
    }

    protected function getTaxonomies(Price $relationItem, $parentId)
    {
        $priceTaxonomies = $relationItem->priceTaxonomy;
        if(empty($priceTaxonomies) || !count($priceTaxonomies)) return null;

        foreach ($priceTaxonomies as $priceTaxonomy)
        {
            $taxonomy = $priceTaxonomy->taxonomy;
            if(!$taxonomy instanceof Taxonomy || $taxonomy->parent_id != $parentId) continue;
            return $taxonomy->id;
        }
        return null;
    }

    protected function getPriceParamField()
    {
        return 'prices_group_price_taxonomies';
    }

    protected function getParamsArray(CmsModelInterface $relationItem = null)
    {
        if(!is_array($this->params) || !count($this->params)) return [];

        foreach ($this->params as $param)
        {
            if(!$param instanceof ProductPriceParamComponent) continue;
            $cloned = clone $param;

            if($relationItem instanceof Price)
            {
                $values = $this->getTaxonomies($relationItem,$param->getParentParamId());
                $cloned->setValue($values);
                $cloned->setPriceId($relationItem->id);
                $cloned->setPriceGroupId($this->priceGroupModel->id);
                $cloned->setField($this->getPriceParamField());
            }
            elseif ($this->priceGroupModel instanceof PriceGroup)
            {
                $cloned->setPriceGroupId($this->priceGroupModel->id);
                $cloned->setField($this->getPriceParamField());
            }
            $params[] = $cloned->toArray();
        }
        return $params;
    }

    protected function getAdditionalData(): array
    {
        return [
            'tableData' => $this->getTableData()->setFieldGroupKey($this->getGroupKey())->toArray(),
            'params' => $this->getParamsArray(),
            'product_id' => $this->productModel->id ?? null,
            'price_group_id' => $this->getModel() ? $this->getModel()->id : '',
            'group_title' => $this->getGroupTitle(),
            'is_deletable_group' => $this->isDeletableGroup(),
            'price_groups_ids_name' => $this->getPriceGroupsIdName(),
            'route_unique_ppt' => route(CoreRouter::ROUTE_CHECK_UNIQUE_PRODUCT_PRICE_GROUP_TAXONOMIES,null,false)
        ];
    }

    private function getRelationItems()
    {
        if(!$this->productModel || !$this->priceGroupModel) return null;

        return Price::where('products_id',$this->productModel->id)->where('price_groups_id',$this->priceGroupModel->id)->orderBy('order')->get();
    }


    protected function getPriceGroupsIdName()
    {
        return 'prices_price_groups_id';
    }

    protected function getGroupTitle()
    {
        return $this->priceGroupModel->info->title ?? '';
    }

    protected function isDeletableGroup()
    {
        return $this->isDeletable;
        //return $this->priceGroupModel->slug !== PriceGroup::DEFAULT;
    }

    /**
     * @param array $params
     * @return $this|FormComponentInterface
     */
    public function setParams(array $params) : FormComponentInterface
    {
        $this->params = $params;
        return $this;
    }

    protected function getGroupKey()
    {
        return $this->priceGroupModel->id;
    }

    protected function isDeletedCurrentGroup()
    {
        if(!$this->isDeletableGroup()) return false;

        if(!request()->get('product_price_panel',false)) return false;

        $groupIds = request()->get($this->getPriceGroupsIdName());

        return !is_array($groupIds) || array_search($this->priceGroupModel->id,$groupIds) === false;
    }

    protected function getTargetIds($requestFieldId)
    {
        $groups = request()->$requestFieldId;
        return $groups[$this->getGroupKey()] ?? [];
    }

    protected function deleteRemovedItems($relationClass, $targetIds)
    {
        $relationClass::where('products_id',$this->productModel->id)
            ->where('price_groups_id',$this->priceGroupModel->id)
            ->whereNotIn('id',$targetIds)->delete();
    }

    /**
     * @param string $relationClass
     * @return CmsModelInterface
     */
    protected function getNewRelationModel($relationClass) : CmsModelInterface
    {
        $item = new $relationClass;
        $item->products_id = $this->productModel->id;
        $item->price_groups_id = $this->priceGroupModel->id;
        return $item;
    }

    /**
     * @param string $requestField
     * @param $order
     * @return mixed|null
     */
    protected function getRequestField($requestField, $order)
    {
        $groups = request()->$requestField;
        return isset($groups[$this->getGroupKey()]) ? $groups[$this->getGroupKey()][$order] ?? null : null;
    }

    protected function storeAdditionalData($item,$isNewItem)
    {

        $priceParamField = $this->getPriceParamField();
        $priceGroups = $this->requestArr[$priceParamField] ?? null;
        if(!is_array($priceGroups) || !isset($priceGroups[$item->price_groups_id])) return true;
        $priceGroup = $priceGroups[$item->price_groups_id];

        if($isNewItem && isset($priceGroup['create']) && is_array($priceGroup['create']))
        {
            $taxonomyIds = [];
            $taxonomies = array_shift($this->requestArr[$priceParamField][$item->price_groups_id]['create']);
            if(!is_array($taxonomies) || !count($taxonomies)) return false;
            foreach ($taxonomies as $taxonomy)
            {
                $taxonomyIds = array_merge($taxonomyIds,explode(',',$taxonomy));
            }
            if(!is_array($taxonomyIds) || !count($taxonomyIds)) return false;

            foreach ($taxonomyIds as $taxonomyId){
                $priceTaxonomy = new PriceTaxonomy();
                $priceTaxonomy->prices_id = $item->id;
                $priceTaxonomy->taxonomies_id = $taxonomyId;
                $priceTaxonomy->save();
            }
        }
        elseif(!$isNewItem && isset($priceGroup['edit']) && is_array($priceGroup['edit']) && isset($priceGroup['edit'][$item->id]))
        {
            $taxonomyIds = [];
            $taxonomies = is_array($priceGroup['edit'][$item->id]) ? $priceGroup['edit'][$item->id] : [];
            foreach ($taxonomies as $taxonomy)
            {
                $taxonomyIds = array_merge($taxonomyIds,explode(',',$taxonomy));
            }
            PriceTaxonomy::where('prices_id',$item->id)->whereNotIn('taxonomies_id',$taxonomyIds)->delete();

            $taxonomiesRelationItems = $item->taxonomies;

            if(!empty($taxonomiesRelationItems) && count($taxonomiesRelationItems))
            {
                foreach ($taxonomiesRelationItems as $taxonomiesRelationItem)
                {
                    $key = array_search($taxonomiesRelationItem->id,$taxonomyIds);
                    if($key !== false){
                        unset($taxonomyIds[$key]);
                    }
                }
            }

            foreach ($taxonomyIds as $taxonomyId){
                $priceTaxonomy = new PriceTaxonomy();
                $priceTaxonomy->prices_id = $item->id;
                $priceTaxonomy->taxonomies_id = $taxonomyId;
                $priceTaxonomy->save();
            }

        }
        return true;
    }

    protected function getExistedRelationItems(string $relation)
    {
        return $this->productModel->$relation->where('price_groups_id',$this->priceGroupModel->id)->keyBy('id');
    }

}
