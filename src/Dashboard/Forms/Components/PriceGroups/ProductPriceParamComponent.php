<?php

namespace VmdCms\Modules\Prices\Dashboard\Forms\Components\PriceGroups;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Input\SelectInterface;
use VmdCms\CoreCms\Dashboard\Forms\Components\Component;
use VmdCms\CoreCms\Traits\Dashboard\Forms\MultiValuable;

class ProductPriceParamComponent extends Component implements SelectInterface
{
    use MultiValuable;

    protected $parentParamId;

    protected $priceId;

    protected $priceGroupId;

    /**
     * @param int $priceId
     * @return $this|FormComponentInterface
     */
    public function setPriceId(int $priceId) : FormComponentInterface
    {
        $this->priceId = $priceId;
        return $this;
    }

    /**
     * @param int $priceGroupId
     * @return $this|FormComponentInterface
     */
    public function setPriceGroupId(int $priceGroupId) : FormComponentInterface
    {
        $this->priceGroupId = $priceGroupId;
        return $this;
    }

    /**
     * @param int $parentParamId
     * @return $this|FormComponentInterface
     */
    public function setParentParamId(int $parentParamId) : FormComponentInterface
    {
        $this->parentParamId = $parentParamId;
        return $this;
    }

    /**
     * @return int
     */
    public function getParentParamId() : int
    {
        return $this->parentParamId;
    }

    protected function getComponentKey(): string
    {
        return "product-price-param-component";
    }

    protected function getValue()
    {
        return $this->value;
    }

    protected function getAdditionalData(): array
    {
        return  [
            'options' => $this->getOptions(),
            'is_id_int' => $this->isIdInt,
            'parent_param_id' => $this->parentParamId,
            'price_id' => $this->priceId,
            'price_group_id' => $this->priceGroupId,
            'created_price_id' => null,
        ];
    }

}
