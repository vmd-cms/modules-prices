<?php

namespace VmdCms\Modules\Prices\Dashboard\Forms\Components\PriceGroups;

use Illuminate\Database\Eloquent\Collection;
use VmdCms\CoreCms\Collections\FormComponentsCollection;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\CustomFormComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Dashboard\Forms\Components\Component;
use VmdCms\CoreCms\Facades\FormTableColumn;
use VmdCms\CoreCms\Traits\Dashboard\Forms\HasComponents;
use VmdCms\Modules\Prices\Models\PriceGroup;
use VmdCms\Modules\Products\Models\Product;
use VmdCms\Modules\Taxonomies\Models\Filter;

class ProductPriceGroupsPanelComponent extends Component implements CustomFormComponentInterface
{
    use HasComponents;

    protected function getComponentKey(): string
    {
        return "product-price-groups-panel-component";
    }

    protected function getAdditionalData(): array
    {
        return [
            'price_groups' => $this->getPriceGroupComponentsArray(),
            'empty_items' => $this->getEmptyPriceGroupComponentsArray()
        ];
    }

    protected function getPriceGroupComponentsArray()
    {
        $this->setPriceGroupComponents($this->getModelPriceGroupRelation());
        $components = [];
        foreach ($this->components->getItemsArray() as $component)
        {
            $components[] = $component->toArray();
        }
        return $components;
    }

    protected function getEmptyPriceGroupComponentsArray()
    {
        $components = $priceGroupsSelect = [];

        $priceGroups = PriceGroup::where('active',1);

        $currentPriceGroups = $this->cmsModel->priceGroups ?? null;
        if(is_countable($currentPriceGroups) && count($currentPriceGroups))
        {
            $currentIds = [];
            foreach ($currentPriceGroups as $currentPriceGroup)
            {
                $currentIds[] = $currentPriceGroup->id;
            }
            $priceGroups->whereNotIn('id',$currentIds);
        }

        $priceGroups = $priceGroups->with('info')->orderBy('order')->get();

        if(!is_countable($priceGroups) || !count($priceGroups))
        {
            return $components;
        }

        foreach ($priceGroups as $priceGroup)
        {
            if(!$priceGroup instanceof PriceGroup) continue;
            $priceGroupsSelect[] = [
                'id' => $priceGroup->id,
                'title' => $priceGroup->info->title ?? null,
            ];
            $components[$priceGroup->id] = $this->getProductPriceGroupComponent($priceGroup)->toArray();
        }
        return [
            'components' => $components,
            'price_groups_select' => $priceGroupsSelect,
            'title' => 'Добавить группу цен'
        ];
    }

    protected function getValue()
    {
        return null;
    }

    public function getStoreCallback(): ?callable
    {
        $this->setPriceGroupComponents($this->getPostPriceGroups());

        return function (){

            foreach ($this->components->getItems() as $component)
            {
                if(!$component instanceof FormComponentInterface) continue;

                if($callable = $component->getStoreCallback())
                {
                    call_user_func($callable);
                }
            }
        };
    }

    protected function getParams()
    {
        $components = [];

        if(!$this->cmsModel instanceof Product) return $components;

        $filters = $this->cmsModel->filters;
        if(empty($filters) || !count($filters)) return $components;

        $filterGroups = $filters->groupBy('parent_id');
        foreach ($filterGroups as $parentId=>$group)
        {
            if(!is_int($parentId)) continue;

            $parent = Filter::where('id',$parentId)->with('info')->first();
            $filterValues = [];
            foreach ($group as $filterItem)
            {
                $filterValues[$filterItem->id] = $filterItem->info->title ?? null;
            }
            $components[] = (new ProductPriceParamComponent('__price_group_params',$parent->info ? $parent->info->title : null))->setEnumValues($filterValues)->setParentParamId($parentId);
        }
        return $components;
    }

    private function getModelPriceGroupRelation()
    {
        return $this->cmsModel->priceGroups ?? null;
    }

    private function getPostPriceGroups()
    {
        $priceGroupIds = [];
        $relation = $this->getModelPriceGroupRelation();
        if(is_countable($relation) && count($relation))
        {
            foreach ($relation as $item)
            {
                $priceGroupIds[$item->id] = $item->id;
            }
        }
        $requestGroupIds = request()->get('prices_price_groups_id');
        if(is_array($requestGroupIds) && count($requestGroupIds))
        {
            foreach ($requestGroupIds as $id)
            {
                $priceGroupIds[$id] = $id;
            }
        }
        if(!count($priceGroupIds)) return null;

        return  PriceGroup::whereIn('id',$priceGroupIds)->with('info')->get();
    }

    private function setPriceGroupComponents(?Collection $priceGroups = null)
    {
        $this->components = new FormComponentsCollection();
        if(!is_countable($priceGroups) || !count($priceGroups))
        {
            return;
        }
        foreach ($priceGroups as $priceGroup)
        {
            if(!$priceGroup instanceof PriceGroup) continue;

            $this->components->appendItem($this->getProductPriceGroupComponent($priceGroup));
        }
    }

    private function getProductPriceGroupComponent(PriceGroup $priceGroup = null)
    {
        return (new ProductPriceGroupComponent('prices'))
            ->setProductModel($this->cmsModel)
            ->setPriceGroupModel($priceGroup)
            ->setParams($this->getParams())
            ->setBottomHelpText('Выберете параметры при которых отображается данная цена:')
            ->setComponents([
                FormTableColumn::numeric('price','Цена')->min(0)->setDefault(0)->setWidth(100),
                FormTableColumn::numeric('discount_percent','%Скидки')->min(0)->setDefault(0)->setWidth(100),
                FormTableColumn::numeric('discount_price','Скидочная цена')->min(0)->setDefault(0)->setWidth(100),
                FormTableColumn::numeric('quantity','Количество')->min(0)->setDefault(0)->setWidth(100),
                FormTableColumn::dateTime('discount_start','Начало акции')->setWidth(250),
                FormTableColumn::dateTime('discount_end','окончание акции')->setWidth(250)
            ]);
    }

}
