<?php

namespace VmdCms\Modules\Prices\Dashboard\Forms\Components\Prices;

use Illuminate\Database\Eloquent\Collection;
use VmdCms\CoreCms\Contracts\Collections\FormComponentsCollectionInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\CustomFormComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\InnerDataTableInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Dashboard\Forms\Components\Component;
use VmdCms\CoreCms\Dashboard\Forms\Tables\Components\Actions\DeleteAction;
use VmdCms\CoreCms\Dashboard\Forms\Tables\Components\Actions\ExpandAction;
use VmdCms\CoreCms\Dashboard\Forms\Tables\Components\ColumnActions;
use VmdCms\CoreCms\Traits\Dashboard\Forms\InnerDataTable;
use VmdCms\Modules\Prices\Dashboard\Forms\Components\PriceGroups\ProductPriceParamComponent;
use VmdCms\Modules\Prices\Models\Price;
use VmdCms\Modules\Prices\Models\PriceTaxonomy;
use VmdCms\Modules\Prices\Services\CoreRouter;
use VmdCms\Modules\Products\Models\Product;
use VmdCms\Modules\Taxonomies\Models\Filter;
use VmdCms\Modules\Taxonomies\Models\Taxonomy;

class ProductPricesPanelComponent extends Component implements CustomFormComponentInterface, InnerDataTableInterface
{
    use InnerDataTable;

    protected $params;

    /**
     * @var FormComponentsCollectionInterface
     */
    protected $dataTableColumns;

    protected function getComponentKey(): string
    {
        return "product-prices-panel-component";
    }

    public function getStoreCallback(): ?callable
    {
        return $this->getStoreTableDataCallback();
    }

    protected function getActionsColumn()
    {
        return (new ColumnActions('__actions', 'Действия'))->setCmsModel($this->cmsModel)
            ->setComponents([
                new ExpandAction('__expand'),
                new DeleteAction('__delete')
            ])->setWidth(120);
    }

    protected function appendExpandedComponents($relationItems)
    {
        if(!$relationItems instanceof Collection) return;

        foreach ($relationItems as $key=>$relationItem)
        {
            if(isset($this->items[$key])) {
                $this->items[$key]['expanded_show'] = false;
                $this->items[$key]['expanded'] =  $this->getParamsArray($relationItem);
            }
        }
    }

    protected function getEmptyItem()
    {
        return array_merge($this->emptyItem, [
            'expanded_show' => true,
            'expanded' => $this->getParamsArray()
        ]);
    }

    protected function getTaxonomies(Price $relationItem, $parentId)
    {
        $priceTaxonomies = $relationItem->priceTaxonomy;
        if(empty($priceTaxonomies) || !count($priceTaxonomies)) return null;

        foreach ($priceTaxonomies as $priceTaxonomy)
        {
            $taxonomy = $priceTaxonomy->taxonomy;
            if(!$taxonomy instanceof Taxonomy || $taxonomy->parent_id != $parentId) continue;
            return $taxonomy->id;
        }
        return null;
    }

    protected function getPriceParamField()
    {
        return 'prices_price_taxonomies';
    }

    protected function getParamsArray(CmsModelInterface $relationItem = null)
    {
        $params = [];
        $modelParams = $this->getParams();

        if(!is_array($modelParams) || !count($modelParams)) return  $params;

        foreach ($modelParams as $param)
        {
            if(!$param instanceof ProductPriceParamComponent) continue;
            $cloned = clone $param;

            if($relationItem instanceof Price)
            {
                $values = $this->getTaxonomies($relationItem,$param->getParentParamId());
                $cloned->setValue($values);
                $cloned->setPriceId($relationItem->id);
                $cloned->setField($this->getPriceParamField());
            }
            $params[] = $cloned->toArray();
        }

        return $params;
    }

    protected function getAdditionalData(): array
    {
        return [
            'tableData' => $this->getTableData()->toArray(),
            'params' => $this->getParamsArray(),
            'product_id' => $this->productModel->id ?? null,
            'route_unique_ppt' => route(CoreRouter::ROUTE_CHECK_UNIQUE_PRODUCT_PRICE_GROUP_TAXONOMIES,null,false)
        ];
    }

    private function getRelationItems()
    {
        if(!$this->cmsModel) return null;

        return Price::where('products_id',$this->cmsModel->id)->orderBy('order')->get();
    }

    protected function getParams()
    {
        $components = [];

        if(!$this->cmsModel instanceof Product) return $components;

        $filters = $this->cmsModel->filters;
        if(empty($filters) || !count($filters)) return $components;

        $filterGroups = $filters->groupBy('parent_id');
        foreach ($filterGroups as $parentId=>$group)
        {
            if(!is_int($parentId)) continue;

            $parent = Filter::where('id',$parentId)->with('info')->first();
            $filterValues = [
                null => 'Выберите фильтр'
            ];
            foreach ($group as $filterItem)
            {
                $filterValues[$filterItem->id] = $filterItem->info->title ?? null;
            }
            $components[] = (new ProductPriceParamComponent('prices_price_taxonomies',$parent->info ? $parent->info->title : null))->setEnumValues($filterValues)->setParentParamId($parentId);
        }
        return $components;
    }

    protected function deleteRemovedItems($relationClass, $targetIds)
    {
        $relationClass::where('products_id',$this->cmsModel->id)
            ->whereNotIn('id',$targetIds)->delete();
    }

    /**
     * @param string $relationClass
     * @return CmsModelInterface
     */
    protected function getNewRelationModel($relationClass) : CmsModelInterface
    {
        $item = new $relationClass;
        $item->products_id = $this->cmsModel->id;
        return $item;
    }

    protected function storeAdditionalData($item,$isNewItem,$index = 0)
    {

        $priceParamField = $this->getPriceParamField();
        $priceFilters = $this->requestArr[$priceParamField] ?? null;

        if(!is_array($priceFilters)) return true;

        if($isNewItem && isset($priceFilters['created']) && is_array($priceFilters['created']))
        {
            $taxonomyIds = $priceFilters['created'][$index] ?? null;

            if(!is_array($taxonomyIds) || !count($taxonomyIds)) return false;

            foreach ($taxonomyIds as $taxonomyId){
                if(empty($taxonomyId)) continue;
                $priceTaxonomy = new PriceTaxonomy();
                $priceTaxonomy->prices_id = $item->id;
                $priceTaxonomy->taxonomies_id = $taxonomyId;
                $priceTaxonomy->save();
            }
        }
        elseif(!$isNewItem && isset($priceFilters['edit']) && is_array($priceFilters['edit']) && isset($priceFilters['edit'][$item->id]))
        {
            $taxonomyIdsArr = is_array($priceFilters['edit'][$item->id]) ? $priceFilters['edit'][$item->id] : [];

            $taxonomyIds = [];
            if(is_countable($taxonomyIdsArr)){
                foreach ($taxonomyIdsArr as $taxItem){
                    if($taxItem) $taxonomyIds[] = $taxItem;
                }
            }

            $taxonomiesRelationItems = $item->taxonomies;

            PriceTaxonomy::where('prices_id',$item->id)->whereNotIn('taxonomies_id',$taxonomyIds)->delete();

            if(!empty($taxonomiesRelationItems) && count($taxonomiesRelationItems))
            {
                foreach ($taxonomiesRelationItems as $taxonomiesRelationItem)
                {
                    $key = array_search($taxonomiesRelationItem->id,$taxonomyIds);
                    if($key !== false){
                        unset($taxonomyIds[$key]);
                    }
                }
            }

            foreach ($taxonomyIds as $taxonomyId){
                if(empty($taxonomyId)) continue;
                $priceTaxonomy = new PriceTaxonomy();
                $priceTaxonomy->prices_id = $item->id;
                $priceTaxonomy->taxonomies_id = $taxonomyId;
                $priceTaxonomy->save();
            }

        }

        return true;
    }

    protected function getExistedRelationItems(string $relation)
    {
        return $this->cmsModel->$relation->keyBy('id');
    }

}
